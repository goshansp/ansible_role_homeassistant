# Ansible Role Homeassistant

Deploy Homeassistant to Ostree based OS. Includes zigbee2mqtt and mosquitto.

# TODO
- wait for mosquitto before starting others
- consolidate file/template copy into single tasks
- archive homeautomation.git
- archive ansible_role_zigbee2mqtt.git / ansible_role_mosquitto.git (remove from pipelines)

# TODO Mosquitto
- converge mosquitto-storage and mosquitto-data
- fix ownership and path of mosquitto/config/password (deprecation of non-root owner pending)

## Passwd
It seems that `mosquitto_passwd` generates reliable hashes while our algo here only works 2 out of 3 times independent of special chars in the passwords.

# TODO Zigbee2MQTT
- wait until mosquitto (0.0.0.0:1883) becomes available?
- z2m: MQTT error: connect ECONNREFUSED 192.168.1.123:1883

## Debug
```
~/.local/share/containers/storage/volumes/zigbee2mqtt-storage/_data/configuration.yaml

podman run -it --name zigbee2mqtt --entrypoint sh --replace docker.io/koenkk/zigbee2mqtt:latest
```